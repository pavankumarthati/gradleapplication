package com.dreamproject.pavan.gradleapplication;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;


public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        new Handler(getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                AccelerateDecelerateInterpolator interpolator = new AccelerateDecelerateInterpolator();
                Animation animation = AnimationUtils.loadAnimation(Main2Activity.this, R.anim.translate_down);
                animation.setDuration(800);
                animation.setInterpolator(interpolator);
                animation.setFillAfter(true);
                Animation animation1 = AnimationUtils.loadAnimation(Main2Activity.this, R.anim.translate_up);
                animation1.setDuration(800);
                animation1.setInterpolator(interpolator);
                animation1.setFillAfter(true);
                findViewById(R.id.tv1).startAnimation(animation);
                findViewById(R.id.tv2).startAnimation(animation1);
            }
        }, 3000);
    }
}